(function() {
    toastr.options.positionClass = "toast-top-full-width";
    $.fn.extend({
        doSongItemSetup: function(item) {
            return $(this).data("new", true).data("id", item.id).data("hash", item.hash).data("changed", false);
        }, insertToList: function($list, index) {
            var $before = null;
            if ($list.children().length !== 0) {
                $before = $list.children().filter(function() {
                    return index < $(this).data("newindex");
                });
            }
            if ($before !== null && $before.length !== 0) {
                return $(this).insertBefore($before.first());
            } else {
                return $(this).appendTo($list);
            }
        }
    });
    var pageLoaded = false, authLoaded = false, level = [], playing = null, player = null,
        playerLoaded = false, songs = [], videoWaiting = false, commandRequestTime = 0, firstLoad = true;
    var updateAuthStatus = function(response) {
        level = response.level;
        authLoaded = true;
    };
    var nextVideo = function() {
        if (!playerLoaded) {
            videoWaiting = true;
            return;
        }
        if (songs.length === 0) {
            playing = null;
            return;
        }
        var nothingPlaying = false;
        var pick = 0, validSongs = [];
        for (var i = 0; i < songs.length; i++) {
            songs[i].score = 0 + Math.floor(Math.pow(2, songs[i].votes));
            if (isFinite(songs[i].score) && songs[i].score > 0) {
                validSongs.push(songs[i]);
                pick += songs[i].score;
            }
        }
        pick = Math.floor(Math.random() * pick);
        for (var i = 0; i < validSongs.length; i++) {
            if (pick < validSongs[i].score) {
                nothingPlaying = playing === null;
                playing = validSongs[i];
                break;
            }
            pick -= validSongs[i].score;
        }
        $("#playing").html(playing.name);
        player.loadVideoById(playing.id);
        if (~level.indexOf("player")) {
            purkkadj.apiRequest({
                "method": "takesong",
                "id": playing.id
            }, function(response) {});
        }
        if (nothingPlaying)
            requestCommand();
    };
    var requestCommand = function() {
        if (playing === null)
            return;
        commandRequestTime = Date.now();
        purkkadj.apiRequest({
            "method": "getcommand",
            "playing": playing.id
        }, function(response) {
            if (response.success) {
                if (response.command === "skip") {
                    if (player.getPlayerState() === YT.PlayerState.PLAYING || player.getPlayerState() === YT.PlayerState.BUFFERING)
                        player.pauseVideo();
                    nextVideo();
                }
            }
            var timeLeft = 500 + commandRequestTime - Date.now();
            if (timeLeft <= 0)
                requestCommand();
            else if (playing !== null)
                setTimeout(requestCommand, timeLeft);
        });
    }
    window.onYouTubeIframeAPIReady = function() {
        player = new YT.Player("player", {
            "width": 640,
            "height": 480,
            "videoId": "kfPx71J-U10",
            "playerVars": {
                "controls": 0,
                "fs": 0,
                "disablekb": 0,
                "showinfo": 0,
                "rel": 0,
                "modestbranding": 1,
                "hl": "fi"
            }, "events": {
                "onReady": function (e) {
                    player.playVideo();
                    setInterval(function() {
                        var formatNum = function(num) {
                            return ("0" + num).slice(-2);
                        };
                        if (typeof player.getPlayerState !== "function")
                            return;
                        if (player.getPlayerState() > 0) {
                            var time = Math.floor(player.getCurrentTime()), dur = Math.floor(player.getDuration());
                            var timeMins = Math.floor(time / 60), timeSecs = time % 60, durMins = Math.floor(dur / 60), durSecs = dur % 60;
                            $("#playtime").html(formatNum(timeMins) + ":" + formatNum(timeSecs) + "/" + formatNum(durMins) + ":" + formatNum(durSecs));
                        }
                    }, 100);
                    playerLoaded = true;
                    if (videoWaiting) {
                        videoWaiting = false;
                        nextVideo();
                    }
                },
                "onStateChange": function (e) {
                    if (e.data === YT.PlayerState.ENDED)
                        nextVideo();
                },
                "onError": function () {
                    toastr.error("Videon lataaminen ep\u00e4onnistui.");
                }
            }
        });
    };
    $(document).ready(function() {
        pageLoaded = true;
        $("html").on("fscreenclose", function() {
            toastr.info("Paina F siirty\u00e4ksesi koko ruudun tilaan.");
        });
        $(document).keypress(function(e) {
            if (e.which === 70 || e.which === 102) {
                $("html").fullscreen({
                    "toggleClass": "fullscreen"
                });
            }
        }).on("fscreenerror", function() {
            toastr.error("Koko ruudun tilaan siirtyminen ep\u00e4onnistui.");
        });
        toastr.info("Paina F siirty\u00e4ksesi koko ruudun tilaan.");
        purkkadj.getUpdates(processResponse);
    });
    var processResponse = function(response) {
        updateAuthStatus(response);
        if ("playerTitle" in response) {
            $("#player-title").html(response.playerTitle);
            $("html").addClass("player-title-shown");
        } else
            $("html").removeClass("player-title-shown");
        $("#vote-url").html(response.voteUrl).attr("href", response.voteUrl);
        $("#vote-qr").qrcode({
            "text": response.voteUrl,
            "quiet": 2,
            "size": 200,
            "fill": "#000",
            "background": "#fff"
        });

        purkkadj.updateSliderList(response.songs, $("#songs"), function(item) {
            return "song-" + purkkadj.encodeId(item.id);
        }, function(item) {
            return "<i>" + item.name + "</i> <span class=\"nobreak\">(" + item.duration + ")</span> &ndash; " +
                "<b class=\"votecount\">" + item.votes + (item.votes == 1 ? " \u00e4\u00e4ni" : " \u00e4\u00e4nt\u00e4") + "</b>";
        }, function(item, $song) {
            $song.find(".votecount").html(item.votes + (item.votes == 1 ? " \u00e4\u00e4ni" : " \u00e4\u00e4nt\u00e4"));
        }, firstLoad);

        purkkadj.updateSliderList(response.played, $("#played"), function(item) {
            return "played-" + purkkadj.encodeId(item.playId);
        }, function(item) {
            return item.timestamp + " &ndash; <i>" + item.name + "</i> <span class=\"nobreak\">(" + item.duration + ")</span>";
        }, function(item, $song) {}, firstLoad);

        songs = response.songs;
        if (playing === null) {
            nextVideo();
        }
        firstLoad = false;
    };
})();
