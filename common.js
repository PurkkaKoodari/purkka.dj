(function() {
    if (sessionStorage.getItem("purkkadj_cache_version") !== "2.3") {
        sessionStorage.removeItem("purkkadj_cache_id");
        sessionStorage.removeItem("purkkadj_cache");
    }
    window.purkkadj = {
        apiRequest: function(params, onload, post) {
            $.ajax({
                "url": "https://purkkadj.pietu1998.net/api.php",
                "method": post ? "POST" : "GET",
                "data": params,
                "dataType": "json",
                "success": onload,
                "error": function(xhr, error, status) {
                    switch (error) {
                    case "timeout":
                        toastr.error("Pyynt\u00f6 aikakatkaistiin.");
                        break;
                    case "error":
                        toastr.error("Pyynt\u00f6 ep\u00e4onnistui" + (status ? ": " + status : "."));
                        break;
                    case "abort":
                        toastr.error("Pyynt\u00f6 keskeytettiin.");
                        break;
                    case "parsererror":
                        toastr.error("J\u00e4sennysvirhe pyynn\u00f6ss\u00e4.");
                        break;
                    }
                    onload({
                        "success": false
                    });
                }
            });
        },
        getUpdates: function(callback) {
            var oldId = sessionStorage.getItem("purkkadj_cache_id");
            var old = sessionStorage.getItem("purkkadj_cache");
            var data = {
                "method": "getstatus"
            };
            if (oldId != null && old != null) {
                old = JSON.parse(old);
                data["cache"] = oldId;
            } else {
                old = {};
            }
            purkkadj.apiRequest(data, function(response) {
                if (response.success) {
                    var parseDelta = function(name, keyId) {
                        if (!(name in response))
                            return;
                        var update = response[name];
                        var result = (name in old) && (!("clear" in update) || !update.clear) ? old[name] : [];
                        for (var i = 0; i < result.length; i++) {
                            var id = result[i][keyId];
                            if (id in update) {
                                if (update[id] === null){
                                    result.splice(i--, 1);
                                } else {
                                    result[i] = update[id];
                                    result[i][keyId] = id;
                                }
                                delete update[id];
                            }
                        }
                        var ids = Object.keys(update);
                        for (var i = 0; i < ids.length; i++) {
                            var id = ids[i];
                            if (id === "clear")
                                continue;
                            update[id][keyId] = id;
                            result.push(update[id]);
                        }
                        response[name] = result;
                    };
                    parseDelta("songs", "id");
                    parseDelta("played", "playId");
                    parseDelta("reported", "id");
                    sessionStorage.setItem("purkkadj_cache_id", response.cacheId);
                    sessionStorage.setItem("purkkadj_cache", JSON.stringify(response));
                    sessionStorage.setItem("purkkadj_cache_version", "2.3");
                    response.songs.sort(function(left, right) {
                        return left.votes > right.votes ? -1 : left.votes < right.votes ? 1 : left.lastvote > right.lastvote ? -1 : left.lastvote < right.lastvote ? 1 : 0;
                    });
                    response.played.sort(function(left, right) {
                        var leftId = parseInt(left.playId.substring(1)), rightId = parseInt(right.playId.substring(1));
                        return leftId > rightId ? -1 : leftId < rightId ? 1 : 0;
                    });
                    if ("reported" in response) {
                        response.reported.sort(function(left, right) {
                            return left.updated > right.updated ? -1 : left.updated < right.updated ? 1 : 0;
                        });
                    }
                    callback(response);
                }
                // TODO debug
                window.t = setTimeout(function() {
                    purkkadj.getUpdates(callback);
                }, 2000);
                window.c = () => clearTimeout(window.t);
            });
        },
        encodeId: function(id) {
            return btoa(id).replace(/\+/g, "_").replace(/\//g, "-").replace(/=/g, "");
        },
        updateSliderList: function(itemArray, $list, idGenerator, contentGenerator, itemUpdater, skipAnimations) {
            var listItems = [];
            var $remove = $list.children();
            for (var i = 0; i < itemArray.length; i++) {
                var item = itemArray[i];
                var $song = $("#" + idGenerator(item));
                if ($song.length > 0) {
                    $remove = $remove.not($song);
                    $song.data("changed", item.hash !== $song.data("hash") && item.hash);
                } else {
                    $song = $("<li></li>").attr("id", idGenerator(item)).html(contentGenerator(item)).doSongItemSetup(item);
                }
                if (itemUpdater !== null)
                    itemUpdater(item, $song);
                $song.data("newindex", i);
                listItems.push($song);
            }
            for (var j = listItems.length - 1; j >= 0; j--) {
                var $item = listItems[j];
                if ($item.data("new")) {
                    $item.data("new", false).insertToList($list, j);
                    if (!skipAnimations)
                        $item.hide().slideDown();
                } else if ($item.data("changed") !== false) {
                    var hash = $item.data("changed");
                    if ($item.data("index") !== j) {
                        var $clone = $item.clone(false).attr("id", "").insertAfter($item);
                        $clone.data("newindex", -1);
                        if ($list.children().length === 0) {
                            $item.appendTo($list);
                        } else {
                            var $before = $list.children().filter(function() {
                                return j < $(this).data("newindex");
                            });
                            if ($before.length !== 0) {
                                $item.insertBefore($before.first());
                            } else {
                                $item.appendTo($list);
                            }
                        }
                        $item.hide().slideDown();
                        $clone.slideUp(function() {
                            $(this).remove();
                        });
                    }
                    $item.data("hash", hash);
                }
                $item.data("index", j);
            }
            $remove.slideUp(500, function() {
               $(this).remove();
            });
        }
    };
})();
