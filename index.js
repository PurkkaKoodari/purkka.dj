(function() {
    toastr.options.positionClass = "toast-top-full-width";
    var showVoteError = function(response) {
        switch (response.reason) {
            case "duplicate":
                toastr.error("Olet jo \u00e4\u00e4nest\u00e4nyt biisi\u00e4 &quot;" + response.name + "&quot;.");
                break;
            case "toolong":
                toastr.error("Biisi &quot;" + response.name + "&quot; on liian pitk\u00e4.");
                break;
            case "banned":
                toastr.error("Biisi &quot;" + response.name + "&quot; on estetty.");
                break;
            case "notfound":
                toastr.error("Biisi\u00e4 ei l\u00f6ydy tai URL on virheellinen.");
                break;
            case "rotation":
                toastr.error("Biisi on jo soinut hiljattain.");
                break;
            default:
                toastr.error("\u00e4\u00e4nest\u00e4minen ep\u00e4onnistui: " + response.reason + ".");
                break;
        }
    };
    $.fn.extend({
        doSongItemSetup: function(item) {
            $(this).data("new", true).data("id", item.id).data("hash", item.hash).data("changed", false).find(".action a").data("id", item.id).click(function(e) {
                e.preventDefault();
                var $this = $(this);
                var $spinner = $("<span></span>").addClass("spinner dark-small");
                var method = $(this).data("method");
                $this.hide().after($spinner);
                purkkadj.apiRequest({
                    "method": method,
                    "id": $(this).data("id")
                }, function(response) {
                    $spinner.hide();
                    if (response.success) {
                        if (!("voted" in response) || response.voted) {
                            $this.parent().find(".done").show();
                            switch (method) {
                            case "vote":
                                toastr.success("\u00c4\u00e4nestit biisi\u00e4 &quot;" + response.name + "&quot;.");
                                break;
                            case "report":
                                toastr.success("Biisi ilmoitettu.");
                                break;
                            case "confirm":
                                toastr.success("Biisi hyv\u00e4ksytty.");
                                break;
                            case "ban":
                                toastr.success("Biisi poistettu.");
                                break;
                            case "nukespam":
                                toastr.success("Poistettu " + response.count + " \u00e4\u00e4nt\u00e4.");
                                break;
                            case "skip":
                                toastr.success("Biisi ohitettu.");
                                break;
                            }
                        } else {
                            $this.show();
                            showVoteError(response);
                        }
                    } else
                        $this.show();
                });
            });
            $(this).find(".action .done").hide();
            return $(this);
        }, updateActions: function(status, voted) {
            $(this).find(".action a").each(function() {
                var enabled = -1;
                switch ($(this).data("method")) {
                case "vote":
                    if (voted !== null)
                        enabled = !voted;
                    break;
                case "ban":
                    enabled = status !== "banned";
                    break;
                case "confirm":
                    enabled = status !== "confirmed";
                    break;
                case "report":
                    break;
                case "nukespam":
                case "skip":
                    enabled = true;
                    break;
                }
                if (enabled === false) {
                    $(this).hide().siblings(".done").show();
                } else if (enabled === true) {
                    $(this).show().siblings(".done").hide();
                }
            });
            return $(this);
        }, insertToList: function($list, index) {
            var $before = null;
            if ($list.children().length !== 0) {
                $before = $list.children().filter(function() {
                    return index < $(this).data("newindex");
                });
            }
            if ($before !== null && $before.length !== 0) {
                return $(this).insertBefore($before.first());
            } else {
                return $(this).appendTo($list);
            }
        }
    });
    var pageLoaded = false, captchaLoaded = false, captchaRendered = false, captchaRequired = false, authLoaded = false,
        level = [], captchaWaiting = false, passcodeAuth = false, passcodeFromHash = null, firstLoad = true;
    var updateAuthStatus = function(response) {
        level = response.level;
        if ("auth" in response)
            passcodeAuth = response.auth === "passcode";
        authLoaded = true;
    };
    var updateRestricted = function() {
        if (pageLoaded && authLoaded && !~level.indexOf("human") && !captchaRequired) {
            captchaRequired = true;
            renderCaptcha();
            $("#captcha, #auth, #code").show();
            $("#captcha-spinner, #auth-spinner, #code-spinner").hide();
            $("#captcha-area").fadeIn();
        }
        $(".restricted").hide();
        $(level.map(function(item) {
            return ".restricted.for-" + item;
        }).join(", ")).not(level.map(function(item) {
            return ".restricted.for-non-" + item;
        }).join(", ")).show();
    };
    var renderCaptcha = function() {
        if (passcodeAuth)  {
            $("#captcha-area-captcha").hide();
            $("#captcha-area-passcode").show();
        } else {
            $("#captcha-area-captcha").show();
            $("#captcha-area-passcode").hide();
        }
        if (!captchaLoaded && !passcodeAuth) {
            captchaWaiting = true;
            return;
        }
        if (captchaRendered && !passcodeAuth) {
            grecaptcha.reset();
            return;
        }
        captchaRendered = true;
        if (passcodeAuth) {
            if (passcodeFromHash !== null) {
                $("#passcode").val(passcodeFromHash);
                $("#code").submit();
                passcodeFromHash = null;
            } else {
                $("#passcode").val("");
            }
        } else {
            grecaptcha.render("captcha", {
                "sitekey": "6Lf-SAsTAAAAAHPqBpZs1Z40R4S22CoWjQNuym_l",
                "theme": "dark",
                "callback": function() {
                    $("#captcha").hide();
                    $("#captcha-spinner").show();
                    purkkadj.apiRequest({
                        "method": "captcha",
                        "code": grecaptcha.getResponse()
                    }, function(response) {
                        if (response.success)
                            updateAuthStatus(response);
                        if (~level.indexOf("human")) {
                            $("#captcha-area").fadeOut();
                            captchaRequired = false;
                            updateRestricted();
                        } else {
                            grecaptcha.reset();
                            $("#captcha").show();
                            $("#captcha-spinner").hide();
                        }
                    }, true);
                }
            });
        }
    };
    window.captchaDone = function() {
        captchaLoaded = true;
        if (captchaWaiting) {
            captchaWaiting = false;
            renderCaptcha();
        }
    };
    $(document).ready(function() {
        pageLoaded = true;
        updateRestricted();
        $("#auth").submit(function(e) {
            e.preventDefault();
            $("#auth").hide();
            $("#auth-spinner").show();
            purkkadj.apiRequest({
                "method": "auth",
                "password": $("#password").val()
            }, function(response) {
                if (response.success)
                    updateAuthStatus(response);
                if (~level.indexOf("human")) {
                    $("#captcha-area").fadeOut();
                    captchaRequired = false;
                    updateRestricted();
                } else {
                    $("#auth").show();
                    $("#auth-spinner").hide();
                }
            }, true);
            $("#password").val("");
        });
        $("#code").submit(function(e) {
            e.preventDefault();
            $("#code").hide();
            $("#code-spinner").show();
            purkkadj.apiRequest({
                "method": "code",
                "code": $("#passcode").val()
            }, function(response) {
                if (response.success)
                    updateAuthStatus(response);
                if (response.success && response.accepted && ~level.indexOf("human")) {
                    $("#captcha-area").fadeOut();
                    captchaRequired = false;
                    updateRestricted();
                } else {
                    $("#code").show();
                    $("#code-spinner").hide();
                    if (response.success && !response.accepted) {
                        switch (response.reason) {
                        case "notfound":
                            toastr.error("P\u00e4\u00e4sykoodi on virheellinen.");
                            break;
                        case "expired":
                            toastr.error("P\u00e4\u00e4sykoodi on vanhentunut.");
                            break;
                        case "used":
                            toastr.error("P\u00e4\u00e4sykoodi on jo k\u00e4ytetty.");
                            break;
                        }
                    }
                }
            }, true);
            $("#passcode").val("");
        });
        $("#config").submit(function(e) {
            e.preventDefault();
            $("#config").hide();
            $("#config-spinner").show();
            purkkadj.apiRequest({
                "method": "setconfig",
                "config": JSON.stringify({
                    "passcodeAuth": $("#config-passcode").prop("checked"),
                    "maxDuration": parseInt($("#config-maxduration").val()),
                    "rotationLength": parseInt($("#config-rotationlength").val()),
                    "playerTitle": $("#config-playertitle").val(),
                    "voteUrl": $("#config-voteurl").val(),
                    "blacklist": $("#config-blacklist").val()
                })
            }, function(response) {
                if (response.success) {
                    $("#config-area").fadeOut();
                    toastr.success("Asetusten tallennus onnistui.");
                } else {
                    $("#config").show();
                    $("#config-spinner").hide();
                    toastr.error("Asetusten tallennus ep\u00e4onnistui.");
                }
            }, true);
        });
        $("#vote").submit(function(e) {
            e.preventDefault();
            var $this = $(this);
            var url = $("#url").val();
            if (!/^(?:(?:https?:\/\/|\/\/)?(?:www\.|m\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=)))?([\w-]{11})(?![\w-])/.test(url)) {
                toastr.error("URL on virheellinen.");
                return;
            }
            $this.find("input, button").prop("disabled", true);
            $("#vote-spinner").css("display", "inline-block");
            purkkadj.apiRequest({
                "method": "vote",
                "id": url
            }, function(response) {
                $("#vote-spinner").hide();
                $this.find("input, button").prop("disabled", false);
                if (!response.success) {
                    toastr.error("\u00c4\u00e4nest\u00e4minen ep\u00e4onnistui.");
                    return;
                }
                if (!response.voted) {
                    showVoteError(response);
                    return;
                }
                $("#url").val("");
                toastr.success("\u00c4\u00e4nestit biisi\u00e4 &quot;" + response.name + "&quot;.");
            });
            
        }).find("input, button").prop("disabled", false);
        $("#add-passcode").click(function(e) {
            e.preventDefault();
            $("#add-passcode").hide();
            $("#passcode-spinner").css("display", "inline-block");
            purkkadj.apiRequest({
                "method": "passcode"
            }, function(response) {
                $("#add-passcode").show();
                $("#passcode-spinner").hide();
                if (!response.success) {
                    toastr.error("P\u00e4\u00e4sykoodin luonti ep\u00e4onnistui.");
                    return;
                }
                $("#new-passcode").html(response.passcode);
                $("#new-passcode-qr").qrcode({
                    "text": location.protocol + "//" + location.hostname + location.pathname + "#code=" + encodeURIComponent(response.passcode),
                    "quiet": 4,
                    "size": 256,
                    "fill": "#000",
                    "background": "#fff"
                });
                $("#passcode-area").fadeIn();
            });
        });
        $("#open-config").click(function(e) {
            e.preventDefault();
            $("#open-config").hide();
            $("#open-config-spinner").css("display", "inline-block");
            purkkadj.apiRequest({
                "method": "getconfig"
            }, function(response) {
                $("#open-config").show();
                $("#open-config-spinner").hide();
                if (!response.success) {
                    toastr.error("Asetusten lataus ep\u00e4onnistui.");
                    return;
                }
                $("#config-passcode").prop("checked", response.config.passcodeAuth);
                $("#config-maxduration").val(response.config.maxDuration);
                $("#config-rotationlength").val(response.config.rotationLength);
                $("#config-playertitle").val(response.config.playerTitle);
                $("#config-voteurl").val(response.config.voteUrl);
                $("#config-blacklist").val(response.config.blacklist);
                $("#config").show();
                $("#config-spinner").hide();
                $("#config-area").fadeIn();
            });
        });
        $("#about").click(function(e) {
            e.preventDefault();
            $("#about-area").fadeIn();
        });
        $(".modal.cancelable .bg, .modal .close").click(function() {
            $(this).closest(".modal").fadeOut();
        });
        $(document).keypress(function(e) {
            if (e.altKey && e.shiftKey && (e.which === 82 || e.which === 114)) {
                purkkadj.apiRequest({
                    "method": "reset"
                }, function(response) {
                    if (!response.success)
                        return;
                    updateAuthStatus(response);
                    updateRestricted();
                });
            }
        });
        purkkadj.getUpdates(processResponse);
    });
    var encodeId = function(id) {
        return btoa(id).replace(/\+/g, "_").replace(/\//g, "-").replace(/=/g, "");
    };
    var processResponse = function(response) {
        updateAuthStatus(response);
        $("#songs-spinner, #played-spinner").hide();
        var duration = [], time = response.maxDuration;
        for (var i = 0; i < 4; i++) {
            var unit = [["pv", 86400], ["h", 3600], ["min", 60], ["sek", 1]][i];
            if (time >= unit[1]) {
                duration.push(Math.floor(time / unit[1]) + " " + unit[0]);
                time %= unit[1];
            }
        }
        $("#maxduration").html(duration.join(" "));
        $("#rotationlength").html(response.rotationLength + 1);

        purkkadj.updateSliderList(response.songs, $("#songs"), function(item) {
            return "song-" + purkkadj.encodeId(item.id);
        }, function(item) {
            return "<b class=\"votecount\">" + item.votes + (item.votes == 1 ? " \u00e4\u00e4ni" : " \u00e4\u00e4nt\u00e4") + "</b>" +
                " &ndash; <span class=\"nobreak\">(" + item.duration + ")</span> " +
                "<a href=\"https://youtu.be/" + item.id + "\" target=\"_blank\"><i>" + item.name + "</i></a>" +
                "<span class=\"action restricted for-human\"> (<a data-method=\"vote\" href=\"javascript:;\">kannata</a><span class=\"done\">kannatettu</span>)</span>" +
                "<span class=\"action restricted for-admin\"> (<a data-method=\"confirm\" href=\"javascript:;\">hyv\u00e4ksy</a><span class=\"done\">hyv\u00e4ksytty</span>)</span>" +
                "<span class=\"action restricted for-human for-non-admin\"> (<a data-method=\"report\" href=\"javascript:;\">ilmoita</a><span class=\"done\">ilmoitettu</span>)</span>" +
                "<span class=\"action restricted for-admin\"> (<a data-method=\"ban\" href=\"javascript:;\">poista</a><span class=\"done\">poistettu</span>)</span>" +
                "<span class=\"action restricted for-admin\"> (<a data-method=\"nukespam\" href=\"javascript:;\">siivoa</a><span class=\"done\">siivottu</span>)</span>";
        }, function(item, $song) {
            $song.find(".votecount").html(item.votes + (item.votes == 1 ? " \u00e4\u00e4ni" : " \u00e4\u00e4nt\u00e4"));
            if ("status" in item)
                $song.updateActions(item.status, item.youvoted);
            else
                $song.updateActions("none", item.youvoted);
        }, firstLoad);

        purkkadj.updateSliderList(response.played, $("#played"), function(item) {
            return "played-" + purkkadj.encodeId(item.playId);
        }, function(item) {
            return item.timestamp + " &ndash; " +
                "<span class=\"nobreak\">(" + item.duration + ")</span> " +
                "<a href=\"https://youtu.be/" + item.id + "\" target=\"_blank\"><i>" + item.name + "</i></a>" +
                "<span class=\"action restricted for-admin\"> (<a data-method=\"confirm\" href=\"javascript:;\">hyv\u00e4ksy</a><span class=\"done\">hyv\u00e4ksytty</span>)</span>" +
                "<span class=\"action restricted for-human for-non-admin\"> (<a data-method=\"report\" href=\"javascript:;\">ilmoita</a><span class=\"done\">ilmoitettu</span>)</span>" +
                "<span class=\"action restricted for-admin\"> (<a data-method=\"ban\" href=\"javascript:;\">poista</a><span class=\"done\">poistettu</span>)</span>" +
                "<span class=\"action restricted for-admin\"> (<a data-method=\"skip\" href=\"javascript:;\">ohita</a><span class=\"done\">ohitettu</span>)</span>";
        }, function(item, $song) {
            if ("status" in item)
                $song.updateActions(item.status, null);
            else
                $song.updateActions("none", null);
        }, firstLoad);

        if ("reported" in response) {
            purkkadj.updateSliderList(response.reported, $("#reports"), function(item) {
                return "reported-" + purkkadj.encodeId(item.id);
            }, function(item) {
                return "<span class=\"nobreak\">(" + item.duration + ")</span> " +
                    "<a href=\"https://youtu.be/" + item.id + "\" target=\"_blank\"><i>" + item.name + "</i></a>" +
                    "<span class=\"action restricted for-admin\"> (<a data-method=\"confirm\" href=\"javascript:;\">hyv\u00e4ksy</a><span class=\"done\">hyv\u00e4ksytty</span>)</span>" +
                    "<span class=\"action restricted for-admin\"> (<a data-method=\"ban\" href=\"javascript:;\">poista</a><span class=\"done\">poistettu</span>)</span>";
            }, function(item, $song) {
                $song.updateActions(item.status, null);
            }, firstLoad);
        }

        updateRestricted();
        firstLoad = false;
    };
    var updatePasscodeFromHash = function() {
        var match = /#code=(.+)/.exec(location.hash);
        if (match !== null) {
            var code = decodeURIComponent(match[1]);
            if (captchaRequired && passcodeAuth) {
                $("#passcode").val(code);
                $("#code").submit();
            } else {
                passcodeFromHash = code;
            }
            history.replaceState(null, "", location.protocol + "//" + location.hostname + location.pathname);
        }
    };
    $(window).on("hashchange", updatePasscodeFromHash);
    updatePasscodeFromHash();
})();
