#Changelog

##2.3

  - Added blacklist regex for song names.
  - Enabled HTTPS by default.
  - Votes now disappear after one hour if nobody else voted for the same song.

##2.2

  - Fixed delta transfer bug.
  - Cache of old versions is now cleared on page load.
  - Added notifications for any successful actions.
  - Added skip function.
  - Added nuke spam function that removes votes on a song by users that only voted on it.

##2.1.1

  - Reports list now works.
  - Played songs are no longer shown after 24 hours.

##2.1

  - All list transfers now only transmit the difference between the current and a cached version.
  - No longer creates 15 GB of traffic per night. (Whoops.)
  - Refactored off a lot of copy-paste code.

##2.0

  - Total rewrite of original code using proper APIs and with more features.
