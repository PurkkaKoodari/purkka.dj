<?php
define('DEBUG', true);
require_once 'config.php';
define('PURKKADJ', true);
header('Content-Type: application/json; charset=utf-8');
header('Cache-Control: no-cache');
header('Pragma: no-cache');
$googleKey = 'AIzaSyDhSq36d797N1MHQ2WCwW1jD_rwZWXcHw8';
set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__ . '/google-api-php-client/src');
$db = null;
$now = time();
function fail($err = false) {
	if (DEBUG) {
		$err .= "\n" . print_r(debug_backtrace(), true);
		error_log("INTERNAL ERROR: {$err}");
	}
	die(json_encode(array(
		'success' => false
	)));
}
function exception_handler($exception) {
	fail($exception->getMessage());
}
function error_handler($errno, $errstr, $errfile, $errline) {
	fail("{$errstr} ({$errno}) at {$errfile} on line {$errline}");
}
function save_config($config) {
	if ($config) {
		if (file_put_contents(__DIR__ . '/config.json', json_encode($config)) === false) fail();
	}
}
set_exception_handler('exception_handler');
set_error_handler('error_handler', E_RECOVERABLE_ERROR | E_WARNING | E_NOTICE | E_USER_WARNING | E_USER_NOTICE);
if (!preg_match("/^mozilla/i", $_SERVER['HTTP_USER_AGENT'])) fail();
if (file_exists(__DIR__ . '/config.json')) {
	$config = json_decode(file_get_contents(__DIR__ . '/config.json'), true);
} else {
	$ssl = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on');
	$port = ((!$ssl && $_SERVER['SERVER_PORT'] == '80') || ($ssl && $_SERVER['SERVER_PORT'] == '443')) ? '' : ':' . $_SERVER['SERVER_PORT'];
	$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'] . $port;
	$voteUrl = preg_replace('#/[^/]*(\?.*)?$#', '/', ($ssl ? 'https://' : 'http://') . $host . $_SERVER['REQUEST_URI']);
	$config = array(
		'passcodeAuth' => false,
		'maxDuration' => 600,
		'rotationLength' => 4,
		'playerTitle' => 'purkka.dj',
		'voteUrl' => $voteUrl
	);
	save_config($config);
}
function any_param($name, $defaultValue = null) {
	if (isset($_GET[$name])) return $_GET[$name];
	if (isset($_POST[$name])) return $_POST[$name];
	if ($defaultValue !== null) return $defaultValue;
	fail();
}
function get_param($name, $defaultValue = null) {
	if (isset($_GET[$name])) return $_GET[$name];
	if ($defaultValue !== null) return $defaultValue;
	fail();
}
function post_param($name, $defaultValue = null) {
	if (isset($_POST[$name])) return $_POST[$name];
	if ($defaultValue !== null) return $defaultValue;
	fail();
}
$method = any_param('method');
$db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASS);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
session_name('purkkadjSession');
session_set_cookie_params(86400, '/', $_SERVER['HTTP_HOST'], false, true);
session_start();
if (!isset($_SESSION['salt']))
	$_SESSION['salt'] = openssl_random_pseudo_bytes(16);
if (!isset($_SESSION['level']))
	$_SESSION['level'] = array();
if (!isset($_SESSION['cache']))
	$_SESSION['cache'] = array();
if (!isset($_SESSION['cacheId']))
	$_SESSION['cacheId'] = '';
function has_perm_level($level) {
	return in_array($level, $_SESSION['level']);
}
function require_perm_level($level) {
	if (!has_perm_level($level)) fail();
}
function succeed($add = array()) {
	die(json_encode(array_merge(array(
		'success' => true
	), $add)));
}
function do_query($sql, $params = array()) {
	global $db;
	$query = $db->prepare($sql);
	foreach ($params as $param) {
		if (!call_user_func_array(array($query, 'bindValue'), $param)) fail(print_r($query->errorInfo(), true));
	}
	if (!$query->execute()) fail(print_r($query->errorInfo(), true));
	return $query;
}
function format_length($length) {
	return sprintf("%d:%02d", floor($length / 60), $length % 60);
}
function cleanup_passcodes() {
	global $now;
	do_query('UPDATE passcodes SET status = \'expired\' WHERE expires <= :time AND status = \'new\'', array(
		array(':time', $now, PDO::PARAM_INT)
	));
}
function get_level_data() {
	global $config;
	if (has_perm_level('human')) {
		return array(
			'level' => $_SESSION['level']
		);
	} else {
		return array(
			'level' => $_SESSION['level'],
			'auth' => $config['passcodeAuth'] ? 'passcode' : 'captcha'
		);
	}
}
function succeed_with_level($add = array()) {
    succeed(array_merge(get_level_data(), $add));
}
function generate_delta(&$result, $name, $keyId, $cacheValid) {
	if (!isset($result[$name]))
		return;
	$cached = $cacheValid && isset($_SESSION['cache'][$name]) ? $_SESSION['cache'][$name] : array();
	$output = array();
	$newCache = array();
	foreach ($result[$name] as $item) {
		$id = $item[$keyId];
		unset($item[$keyId]);
		$resend = true;
		if (isset($cached[$id])) {
			$cachedItem = $cached[$id];
			$resend = false;
			foreach ($item as $key => $value) {
				if (!isset($cachedItem[$key]) || $value !== $cachedItem[$key]) {
					$resend = true;
					break;
				}
			}
			unset($cached[$id]);
		}
		$newCache[$id] = $item;
		if ($resend)
			$output[$id] = $item;
	}
	foreach ($cached as $id => $item) {
		$output[$id] = null;
	}
	if (!$cacheValid)
		$output['clear'] = true;
	$result[$name] = $output;
	$_SESSION['cache'][$name] = $newCache;
	return !empty($output);
}
switch ($method) {
case 'captcha':
	if ($config['passcodeAuth']) fail();
	$result = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, stream_context_create(array(
		'http' => array(
			'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			'method'  => 'POST',
			'content' => http_build_query(array(
				'secret' => RECAPTCHA_SECRET,
				'response' => post_param('code'),
				'remoteip' => $_SERVER['REMOTE_ADDR']
			)),
		),
	))));
	if ($result->success)
		$_SESSION['level'] = array_unique(array_merge($_SESSION['level'], array('human')));
	succeed_with_level();
case 'auth':
	$pass = post_param('password');
	if (md5($pass) === ADMIN_PASSWORD) {
		$_SESSION['level'] = array_unique(array_merge($_SESSION['level'], array('human', 'admin')));
	} elseif (md5($pass) === PLAYER_PASSWORD) {
		$_SESSION['level'] = array_unique(array_merge($_SESSION['level'], array('human', 'player')));
	}
	succeed_with_level();
case 'code':
	if (!$config['passcodeAuth']) fail();
	$passcode = post_param('code');
	cleanup_passcodes();
	$query = do_query('SELECT status FROM passcodes WHERE passcode = :code', array(
		array(':code', $passcode)
	));
	if ($query->rowCount() === 0) {
		succeed_with_level(array(
			'accepted' => false,
			'reason' => 'notfound'
		));
	}
	$row = $query->fetch(PDO::FETCH_ASSOC);
	if ($row['status'] !== 'new') {
		succeed_with_level(array(
			'accepted' => false,
			'reason' => $row['status']
		));
	}
	$query = do_query('UPDATE passcodes SET status = \'used\' WHERE passcode = :code AND status = \'new\'', array(
		array(':code', $passcode)
	));
	if ($query->rowCount() > 0) {
		$_SESSION['level'] = array_unique(array_merge($_SESSION['level'], array('human')));
		succeed_with_level(array(
			'accepted' => true
		));
	} else {
		succeed_with_level(array(
			'accepted' => false,
			'reason' => 'used'
		));
	}
case 'reset':
	$_SESSION['level'] = array();
	succeed_with_level();
case 'getstatus':
	$result = get_level_data();
	$result['maxDuration'] = $config['maxDuration'];
	$result['rotationLength'] = $config['rotationLength'];
	$result['voteUrl'] = $config['voteUrl'];
	if (has_perm_level('player'))
		$result['playerTitle'] = $config['playerTitle'];
	$query = do_query('SELECT songs.id AS id, songs.name AS name, songs.duration AS duration, '
		. 'COUNT(votes.id) AS votecount, SUM(votes.user = :id) AS youvoted, songs.status AS status, '
		. 'MAX(votes.timestamp) as lastvote FROM songs, votes '
		. 'WHERE songs.status <> \'banned\' AND songs.duration <= :duration AND songs.id = votes.id '
		. 'GROUP BY votes.id ORDER BY COUNT(votes.id) DESC, MAX(votes.timestamp) DESC', array(
		array(':duration', $config['maxDuration']),
		array(':id', session_id())
	));
	$result['songs'] = array();
	foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
		$item = array(
			'id' => $row['id'],
			'name' => $row['name'],
			'duration' => format_length(intval($row['duration'])),
			'votes' => intval($row['votecount']),
			'youvoted' => intval($row['youvoted']) > 0,
			'lastvote' => intval($row['lastvote']),
			'hash' => sha1("{$_SESSION['salt']}_{$row['votecount']}_{$row['lastvote']}")
		);
		if (has_perm_level('admin')) {
			$item['status'] = $row['status'];
		}
		$result['songs'][] = $item;
	}
	$query = do_query('SELECT played.playId AS playId, played.id AS id, songs.name AS name, '
		. 'songs.duration AS duration, played.timestamp AS timestamp, songs.status AS status '
		. 'FROM played, songs WHERE songs.id = played.id AND songs.status <> \'banned\' '
		. 'AND played.timestamp > :time - 84600 '
		. 'ORDER BY played.timestamp DESC LIMIT :limit', array(
		array(':limit', max(1, $config['rotationLength']), PDO::PARAM_INT),
		array(':time', time(), PDO::PARAM_INT)
	));
	$result['played'] = array();
	foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
		$item = array(
			'playId' => 'p' . $row['playId'],
			'id' => $row['id'],
			'name' => $row['name'],
			'duration' => format_length(intval($row['duration'])),
			'timestamp' => date("H:i", intval($row['timestamp'])),
			'hash' => sha1("{$_SESSION['salt']}_{$row['timestamp']}")
		);
		if (has_perm_level('admin')) {
			$item['status'] = $row['status'];
		}
		$result['played'][] = $item;
	}
	if (has_perm_level('admin')) {
		$query = do_query('SELECT id, name, duration, status, updated FROM songs '
			. 'WHERE status = \'reported\' ORDER BY updated ASC');
		$result['reported'] = array();
		foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $row) {
			$item = array(
				'id' => $row['id'],
				'name' => $row['name'],
				'duration' => format_length(intval($row['duration'])),
				'hash' => sha1("{$_SESSION['salt']}_{$row['updated']}"),
				'updated' => intval($row['updated']),
				'status' => $row['status']
			);
			$result['reported'][] = $item;
		}
	}
	$cacheId = any_param('cache', '');
	$cacheValid = $_SESSION['cacheId'] === $cacheId;
	$changed = generate_delta($result, 'songs', 'id', $cacheValid);
	$changed = generate_delta($result, 'played', 'playId', $cacheValid) || $changed;
	$changed = generate_delta($result, 'reported', 'id', $cacheValid) || $changed;
	if ($changed)
		$_SESSION['cacheId'] = md5(uniqid());
	$result['cacheId'] = $_SESSION['cacheId'];
	succeed($result);
case 'getcommand':
	$playing = any_param('playing');
	$query = do_query('SELECT duration, status, skip FROM songs WHERE id = :id', array(
		array(':id', $playing)
	));
	if ($query->rowCount() === 0) fail();
	$row = $query->fetch(PDO::FETCH_ASSOC);
	$action = ($row['status'] === 'banned' || intval($row['skip']) || intval($row['duration']) > $config['maxDuration']) ? 'skip' : 'none';
	if (has_perm_level('player')) {
		do_query('UPDATE songs SET skip = FALSE WHERE skip AND id = :id', array(
			array(':id', $playing)
		));
	}
	succeed(array(
		'command' => $action
	));
}
require_perm_level('human');
function set_song_status($id, $status, $additional = '') {
	global $now;
	do_query('UPDATE songs SET status = :status, updated = :time WHERE id = :id' . $additional, array(
		array(':status', $status),
		array(':time', $now, PDO::PARAM_INT),
		array(':id', $id)
	));
}
/**
 *  @author  Stephan Schmitz <eyecatchup@gmail.com>
 */
function parse_youtube_url($url) {
	$pattern = '#^(?:(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com/(?:embed/|v/|watch\?v=|watch\?.+&v=)))?([\w-]{11})(?![\w-])#';
	preg_match($pattern, $url, $matches);
	return isset($matches[1]) ? $matches[1] : false;
}
switch ($method) {
case 'takesong':
	require_perm_level('player');
	$id = any_param('id');
	do_query('DELETE FROM votes WHERE id = :id', array(
		array(':id', $id)
	));
	do_query('DELETE votes FROM votes JOIN (SELECT id FROM votes GROUP BY id HAVING COUNT(*) = 1 AND MAX(timestamp) < :time) temp ON votes.id = temp.id', array(
		array(':time', time() - 3600, PDO::PARAM_INT)
	));
	do_query('UPDATE songs SET skip = FALSE WHERE skip AND id = :id', array(
		array(':id', $id)
	));
	do_query('INSERT INTO played (id, timestamp) VALUES (:id, :time)', array(
		array(':id', $id),
		array(':time', $now, PDO::PARAM_INT)
	));
	succeed();
case 'vote':
	$id = parse_youtube_url(any_param('id'));
	if ($id === false) {
		succeed(array(
			'voted' => false,
			'reason' => 'notfound'
		));
	}
	$query = do_query('SELECT name, status, duration FROM songs WHERE id = :id', array(
		array(':id', $id)
	));
	if ($query->rowCount() === 0) {
		require_once 'Google/autoload.php';
		require_once 'Google/Client.php';
		require_once 'Google/Service/YouTube.php';
		$client = new Google_Client();
		$client->setDeveloperKey($googleKey);
		$youtube = new Google_Service_YouTube($client);
		$info = $youtube->videos->listVideos('contentDetails,snippet', array(
			'id' => $id
		));
		if (count($info) === 0) {
			succeed(array(
				'voted' => false,
				'reason' => 'notfound'
			));
		}
		$video = $info[0];
		$name = $video['snippet']['title'];
		if (preg_match('@' . $config['blacklist'] . '@i', $name)) {
			succeed(array(
				'voted' => false,
				'reason' => 'spam'
			));
		}
		$durationStr = $video['contentDetails']['duration'];
		preg_match_all('/(\d+)([DHMS])/', $durationStr, $matches, PREG_SET_ORDER);
		$duration = 0;
		$units = array(
			'D' => 86400,
			'H' => 3600,
			'M' => 60,
			'S' => 1
		);
		foreach ($matches as $match)
			$duration += intval($match[1]) * $units[$match[2]];
		$status = 'none';
		do_query('INSERT INTO songs (id, name, duration, updated) VALUES (:id, :name, :duration, :time) '
			. 'ON DUPLICATE KEY UPDATE name = :name2, duration = :duration2, updated = :time2', array(
			array(':id', $id),
			array(':name', $name),
			array(':duration', $duration, PDO::PARAM_INT),
			array(':time', $now, PDO::PARAM_INT),
			array(':name2', $name),
			array(':duration2', $duration, PDO::PARAM_INT),
			array(':time2', $now, PDO::PARAM_INT)
		));
	} else {
		$row = $query->fetch(PDO::FETCH_ASSOC);
		$name = $row['name'];
		$duration = intval($row['duration']);
		$status = $row['status'];
	}
	if ($status === 'banned') {
		succeed(array(
			'voted' => false,
			'reason' => 'banned',
			'name' => $name
		));
	}
	if ($duration > $config['maxDuration']) {
		succeed(array(
			'voted' => false,
			'reason' => 'toolong',
			'name' => $name
		));
	}
	if ($config['rotationLength'] > 0) {
		$query = do_query('SELECT * FROM (SELECT * FROM played ORDER BY timestamp DESC LIMIT :limit) AS temp '
			. 'WHERE id = :id', array(
			array(':limit', $config['rotationLength'], PDO::PARAM_INT),
			array(':id', $id)
		));
		if ($query->rowCount() > 0) {
			succeed(array(
				'voted' => false,
				'reason' => 'rotation',
				'name' => $name
			));
		}
	}
	$query = do_query('INSERT IGNORE INTO votes (id, user, timestamp) VALUES (:id, :user, :time)', array(
		array(':id', $id),
		array(':user', session_id()),
		array(':time', $now, PDO::PARAM_INT)
	));
	if ($query->rowCount() === 0) {
		succeed(array(
			'voted' => false,
			'reason' => 'duplicate',
			'name' => $name
		));
	}
	succeed(array(
		'voted' => true,
		'name' => $name
	));
case 'report':
	$id = any_param('id');
	set_song_status($id, 'reported', ' AND status = \'none\'');
	succeed();
case 'ban':
	require_perm_level('admin');
	$id = any_param('id');
	set_song_status($id, 'banned');
	do_query('DELETE FROM votes WHERE id = :id', array(
		array(':id', $id)
	));
	succeed();
case 'confirm':
	require_perm_level('admin');
	$id = any_param('id');
	set_song_status($id, 'confirmed');
	succeed();
case 'skip':
	require_perm_level('admin');
	$id = any_param('id');
	do_query('UPDATE songs SET skip = TRUE WHERE id = :id', array(
		array(':id', $id)
	));
	succeed();
case 'nukespam':
	require_perm_level('admin');
	$id = any_param('id');
	$query = do_query('DELETE FROM votes WHERE id = :id AND (SELECT COUNT(*) FROM (SELECT * FROM votes) AS votes2 WHERE votes.user = votes2.user) = 1', array(
		array(':id', $id)
	));
	succeed(array(
		'count' => $query->rowCount()
	));
case 'passcode':
	require_perm_level('admin');
	do {
		if (defined('WORD_LIST')) {
			$words = file(__DIR__ . '/' . WORD_LIST, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			$passcode = implode(' ', array_intersect_key($words, array_flip(array_rand($words, 2))));
		} else {
			$passcode = implode('', array_rand('abcdefghijklmnopqrstuvwxyz', 12));
		}
		$query = do_query('INSERT IGNORE INTO passcodes (passcode, expires) VALUES (:code, :expires)', array(
			array(':code', $passcode),
			array(':expires', $now + 300, PDO::PARAM_INT)
		));
	} while ($query->rowCount() === 0);
	cleanup_passcodes();
	succeed(array(
		'passcode' => $passcode
	));
case 'getconfig':
	require_perm_level('admin');
	succeed(array(
		'config' => $config
	));
case 'setconfig':
	require_perm_level('admin');
	$newConfig = json_decode(post_param('config'), true);
	if (!isset($newConfig['passcodeAuth']) || !is_bool($newConfig['passcodeAuth'])) fail();
	if (!isset($newConfig['maxDuration']) || !is_int($newConfig['maxDuration']) || $newConfig['maxDuration'] < 10) fail();
	if (!isset($newConfig['rotationLength']) || !is_int($newConfig['rotationLength']) || $newConfig['rotationLength'] < 0) fail();
	if (!isset($newConfig['playerTitle']) || !is_string($newConfig['playerTitle'])) fail();
	if (!isset($newConfig['voteUrl']) || !is_string($newConfig['voteUrl'])) fail();
	if (!isset($newConfig['blacklist']) || !is_string($newConfig['blacklist']) || strpos($newConfig['blacklist'], '@') !== false) fail();
	save_config($newConfig);
	succeed();
default:
	fail();
}
