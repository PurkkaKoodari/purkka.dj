-- create_tables.sql
--
-- Creates the tables used by purkka.dj

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `passcodes` (
  `passcodeId` int(11) NOT NULL AUTO_INCREMENT,
  `passcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires` int(11) NOT NULL,
  `status` enum('new','expired','used','') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'new',
  PRIMARY KEY (`passcodeId`),
  UNIQUE KEY `passcode` (`passcode`)
)  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `played` (
  `playId` int(11) NOT NULL AUTO_INCREMENT,
  `id` char(11) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  PRIMARY KEY (`playId`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `songs` (
  `id` char(11) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `status` enum('none','reported','banned','confirmed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `skip` boolean NOT NULL DEFAULT FALSE,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `votes` (
  `id` char(11) COLLATE utf8_unicode_ci NOT NULL,
  `user` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  PRIMARY KEY (`id`,`user`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

