-- reset_complete.sql
--
-- Clears all data

TRUNCATE TABLE `passcodes`;
TRUNCATE TABLE `votes`;
TRUNCATE TABLE `played`;
TRUNCATE TABLE `songs`;
