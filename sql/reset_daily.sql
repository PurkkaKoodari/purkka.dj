-- reset_daily.sql
--
-- Clears any votes and passcodes

TRUNCATE TABLE `passcodes`;
TRUNCATE TABLE `votes`;
